﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class ShakerContentsScripts : MonoBehaviour
{
    [SerializeField] protected DictionaryOfStringAndInt _ingredientsNeeded = new DictionaryOfStringAndInt();
    [SerializeField] protected DictionaryOfStringAndInt _ingredientsInShaker = new DictionaryOfStringAndInt();
    [SerializeField] protected int _neededGrapefruitPeeled = 0;
    [SerializeField] protected int _neededGrapefruitWhole = 0;
    [SerializeField] protected int _neededGrapefruitHalved = 0;
    [SerializeField] protected int _neededGrenadineBottle = 0;
    [SerializeField] protected int _neededLemonHalved = 0;
    [SerializeField] protected int _neededLemonWhole = 0;
    [SerializeField] protected int _neededMangoHalved = 0;
    [SerializeField] protected int _neededMangoWhole = 0;
    [SerializeField] protected int _neededOrangeHalved = 0;
    [SerializeField] protected int _neededOrangeWhole = 0;
    [SerializeField] protected int _neededPineappleHalved = 0;
    [SerializeField] protected int _neededPineappleWhole = 0;
    [SerializeField] protected int _neededRaspberry = 0;
    [SerializeField] protected int _neededBasil = 0;
    [SerializeField] protected int _neededCinnamon = 0;
    [SerializeField] protected int _neededMint = 0;
    [SerializeField] protected int _neededPinkPeppercorn = 0;
    [SerializeField] protected int _neededRedBasil = 0;
    [SerializeField] protected int _neededIceCubeCrushed = 0;
    [SerializeField] protected int _neededIceCubeWhole = 0;
    [SerializeField] protected int _neededSalt = 0;
    [SerializeField] protected int _neededSugar = 0;
    [SerializeField] protected int _neededElderflowerJuiceBottle = 0;
    [SerializeField] protected int _neededGrapefruitJuiceBottle = 0;
    [SerializeField] protected int _neededLimeJuiceBottle = 0;
    [SerializeField] protected int _neededLemonJuiceBottle = 0;
    [SerializeField] protected int _neededHoneyBottle = 0;
    [SerializeField] protected int _neededWaterBottle = 0;


    private GameManagerScript _gameManager;

    public GameObject NeededHolder;

    public int CountPassedObjects;


    private bool readyToShake = false;

    //for tutorial
    private bool _ice;
    private bool _sugar;
    public static Action TutorialIce;
    public static Action TutorialSugar;

    private void Start()
    {
        _ingredientsNeeded.Add("Grapefruit(Peeled)", _neededGrapefruitPeeled);
        _ingredientsNeeded.Add("Grapefruit(Whole)", _neededGrapefruitWhole);
        _ingredientsNeeded.Add("Lemon(Halved)", _neededLemonHalved);
        _ingredientsNeeded.Add("Lemon(Whole)", _neededLemonWhole);
        _ingredientsNeeded.Add("Mango(Halved)", _neededMangoHalved);
        _ingredientsNeeded.Add("Mango(Whole)", _neededMangoWhole);
        _ingredientsNeeded.Add("Orange(Halved)", _neededOrangeHalved);
        _ingredientsNeeded.Add("Orange(Whole)", _neededOrangeWhole);
        _ingredientsNeeded.Add("Pineapple(Halved)", _neededPineappleHalved);
        _ingredientsNeeded.Add("Pineapple(Whole)", _neededPineappleWhole);
        _ingredientsNeeded.Add("Raspberry", _neededRaspberry);
        _ingredientsNeeded.Add("Basil", _neededBasil);
        _ingredientsNeeded.Add("Cinnamon", _neededCinnamon);
        _ingredientsNeeded.Add("Mint", _neededMint);
        _ingredientsNeeded.Add("PinkPeppercorn", _neededPinkPeppercorn);
        _ingredientsNeeded.Add("RedBasil", _neededRedBasil);
        _ingredientsNeeded.Add("IceCube(Crushed)", _neededIceCubeCrushed);
        _ingredientsNeeded.Add("IceCube(Whole)", _neededIceCubeWhole);
        _ingredientsNeeded.Add("Salt", _neededSalt);
        _ingredientsNeeded.Add("Sugar", _neededSugar);
        _ingredientsNeeded.Add("GrenadineBottle", _neededGrenadineBottle);
        _ingredientsNeeded.Add("ElderflowerJuiceBottle", _neededElderflowerJuiceBottle);
        _ingredientsNeeded.Add("GrapefruitJuiceBottle", _neededGrapefruitJuiceBottle);
        _ingredientsNeeded.Add("LimeJuiceBottle", _neededLimeJuiceBottle);
        _ingredientsNeeded.Add("HoneyBottle", _neededHoneyBottle);
        _ingredientsNeeded.Add("LemonJuiceBottle", _neededLemonJuiceBottle);
        _ingredientsNeeded.Add("WaterBottle", _neededWaterBottle);



        InitializeIngredientsInShaker();

        _gameManager = FindObjectOfType<GameManagerScript>();

        ContentUpdater();

        _ice = true;
        _sugar = true;
    }


    private void Update()
    {
        ContentUpdater();
        ForTutorial();
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entered");
        if (!other.CompareTag("MuddlerTool"))
        {
            AddIngredient(other.gameObject.name.Substring(0, other.gameObject.name.LastIndexOf("(")));

            CountPassedObjects++;

            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
    }


    public virtual void AddIngredient(string IngredientName)
    {
        FindObjectOfType<AudioManagerScript>().PlayDropSolid();
        Debug.Log(IngredientName);
        _ingredientsInShaker[IngredientName]++;
        if (_ingredientsInShaker[IngredientName] > _ingredientsNeeded[IngredientName])
        {
            Debug.Log("You Added Too Much " + IngredientName + "!");
            FailStage();
            readyToShake = false;
        }
        else
        {
            FindObjectOfType<GameManagerScript>().SuccesStage();
        }
        if (CheckEqual())
        {
            Debug.Log("Done");
            readyToShake = true;
        }
    }

    public bool GetReadyToShake()
    {
        return readyToShake;
    }

    protected bool CheckEqual()
    {
        bool equal = false;
        if (_ingredientsNeeded.Count == _ingredientsInShaker.Count) // Require equal count.
        {
            equal = true;
            foreach (var pair in _ingredientsNeeded)
            {
                int value;
                if (_ingredientsInShaker.TryGetValue(pair.Key, out value))
                {
                    // Require value be equal.
                    if (value != pair.Value)
                    {
                        equal = false;
                        break;
                    }
                }
                else
                {
                    // Require key be present.
                    equal = false;
                    break;
                }
            }
        }
        return equal;
    }

    protected void InitializeIngredientsInShaker()
    {
        _ingredientsInShaker.Clear();

        for (int i = 0; i < _ingredientsNeeded.Count; i++)
        {
            string key = _ingredientsNeeded.ElementAt(i).Key;
            _ingredientsInShaker.Add(key, 0);
        }
    }

    public virtual void FailStage()
    {
        _gameManager.FailStage(1);
        InitializeIngredientsInShaker();

    }

    public void ContentUpdater()
    {

        foreach (Transform child in NeededHolder.transform)
        {
            GameObject.Destroy(child.gameObject);
        }


        for (int i = 0; i < _ingredientsNeeded.Count; i++)
        {
            string key = _ingredientsNeeded.ElementAt(i).Key;
            int value = _ingredientsNeeded.ElementAt(i).Value - _ingredientsInShaker[key];
            key = "Needed" + key;

            //Debug.Log(key);
            //Debug.Log(value);

            for (int j = 0; j < value; j++)
            {
                GameObject needed = Resources.Load(key) as GameObject;
                GameObject spawnNeeded = Instantiate(needed);
                spawnNeeded.transform.SetParent(NeededHolder.transform);
                spawnNeeded.transform.localScale = Vector3.one;
            }
        }


    }

    //for tutorial
    private void ForTutorial()
    {
        if (_ice && _ingredientsInShaker["IceCube(Whole)"] == 2)
        {
            if (TutorialIce != null)
                TutorialIce();
            _ice = false;
        }
        if (_sugar && _ingredientsInShaker["Sugar"] == 1)
        {
            if (TutorialSugar != null)
                TutorialSugar();
            _sugar = false;
        }
    }
}
