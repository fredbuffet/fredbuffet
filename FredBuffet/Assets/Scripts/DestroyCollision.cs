﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCollision : MonoBehaviour {



    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("enter");

        if (other.gameObject.GetComponent<FruitDestroy>() != null)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
    }




    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<FruitDestroy>() != null)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = false;
    }


}
