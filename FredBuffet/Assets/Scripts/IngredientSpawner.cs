﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IngredientSpawner : MonoBehaviour
{
    public GameObject[] HerbArr;
    public GameObject[] FruitArr;
    public GameObject[] LiquidsArr;
    public GameObject[] MiscArr;

    public bool _textBoxInScene = false;

    private Quaternion _spawnRotation = new Quaternion(0, 0, 0, 1);

    public string _selectedLiquidName = "no liquid selected";

    public Text _textBox;

    private GameObject _gameManager;

    void Start()
    {
        _gameManager = this.gameObject;
    }

    private void Update()
    {
        if (_textBoxInScene) _selectedLiquidName = _textBox.text;

    }
    public void HerbPressed(Text textBox)
    {
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        SpawnObject(HerbArr, EventSystem.current.currentSelectedGameObject.name, EventSystem.current.currentSelectedGameObject.transform);
        textBox.text = "no liquid selected";
    }

    public void FruitPressed(Text textBox)
    {
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        SpawnObject(FruitArr, EventSystem.current.currentSelectedGameObject.name, EventSystem.current.currentSelectedGameObject.transform);
        textBox.text = "no liquid selected";
    }
    public void FruitPressed()
    {
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        SpawnObject(FruitArr, EventSystem.current.currentSelectedGameObject.name, EventSystem.current.currentSelectedGameObject.transform);
    }

    public void FruitPressedSlice()
    {
        if (GameObject.FindGameObjectWithTag("SliceAble") == null)
        {
            Debug.Log(EventSystem.current.currentSelectedGameObject.name);
            SpawnObjectPrefab(FruitArr, EventSystem.current.currentSelectedGameObject.name);
        }
    }

    public void LiquidPressed(Text textBox)
    {
        SetLiquidObject(EventSystem.current.currentSelectedGameObject.name);
        textBox.text = _selectedLiquidName;
        Debug.Log(_selectedLiquidName);
    }

    public void MiscPressed()
    {
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        SpawnObject(MiscArr, EventSystem.current.currentSelectedGameObject.name, EventSystem.current.currentSelectedGameObject.transform);
    }

    void SpawnObject(GameObject[] array, string objectName, Transform pos)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].name == objectName)
            {
                FindObjectOfType<AudioManagerScript>().PlayFruitSpawningSound();
                if (GameObject.FindGameObjectWithTag("JuiceAble") != null)
                    Destroy(GameObject.FindGameObjectWithTag("JuiceAble"));

                Vector3 tempPos = pos.position;
                tempPos.x = tempPos.x;
                Instantiate(array[i], tempPos, _spawnRotation);
            }
        }
    }

    void SpawnObjectPrefab(GameObject[] array, string objectName)
    {

        for (int i = 0; i < array.Length; i++)
        {

            if (array[i].name == objectName)
            {
                FindObjectOfType<AudioManagerScript>().PlayFruitSpawningSound();

                Instantiate(array[i]);
            }
        }
    }

    void SetLiquidObject(string objectName)
    {
        _selectedLiquidName = objectName;
    }


}
