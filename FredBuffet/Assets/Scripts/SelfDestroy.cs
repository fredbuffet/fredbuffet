﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    //self destroy after set time
    public int DestroyTimer = 2;

    void Awake()
    {
        Destroy(this.gameObject, DestroyTimer);
    }

}
