﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientSpawnerV3 : MonoBehaviour
{
    public GameObject RespawnThis;

    public GameObject GetPos;
    private Transform PosTransform;
    private Quaternion RotationQuaternion;




    void Start()
    {
        PosTransform = GetPos.transform;
        RotationQuaternion = GetPos.transform.rotation;

        Instantiate(RespawnThis, PosTransform.position, RotationQuaternion);
    }

    public void OnTriggerExit2D(Collider2D other)
    {


        //Debug.Log(other.name);
        if ((other.name == RespawnThis.name || other.name == RespawnThis.name + "(Clone)"))
        {
            Instantiate(RespawnThis, PosTransform.position, RotationQuaternion);
        }


    }

}
