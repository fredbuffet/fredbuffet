﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Image _imageLevel1;
    [SerializeField] private Image _imageLevel2;
    [SerializeField] private Image _imageLevel3;
    [SerializeField] private Image _imageLevel4;
    [SerializeField] private Image _imageLevel5;
    [SerializeField] private Image _imageTutorial;


    [SerializeField] private Sprite _cocktail1_1;
    [SerializeField] private Sprite _cocktail1_2;
    [SerializeField] private Sprite _cocktail1_3;
    [SerializeField] private Sprite _cocktail1_4;
                                    
    [SerializeField] private Sprite _cocktail2_1;
    [SerializeField] private Sprite _cocktail2_2;
    [SerializeField] private Sprite _cocktail2_3;
    [SerializeField] private Sprite _cocktail2_4;

    [SerializeField] private Sprite _cocktail3_1;
    [SerializeField] private Sprite _cocktail3_2;
    [SerializeField] private Sprite _cocktail3_3;
    [SerializeField] private Sprite _cocktail3_4;

    [SerializeField] private Sprite _cocktail4_1;
    [SerializeField] private Sprite _cocktail4_2;
    [SerializeField] private Sprite _cocktail4_3;
    [SerializeField] private Sprite _cocktail4_4;

    [SerializeField] private Sprite _cocktail5_1;
    [SerializeField] private Sprite _cocktail5_2;
    [SerializeField] private Sprite _cocktail5_3;
    [SerializeField] private Sprite _cocktail5_4;

    [SerializeField] private Sprite _cocktailTutorial;

    private void Start()
    {
        if (FindObjectOfType<GameManagerScript>() != null)
            SetLevelStickers();
    }


    public void LoadLevelselectScene()
    {
        SceneManager.LoadScene(1);
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadPreviousScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

    }

    public void LoadLevelOne()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(2);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(1);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void LoadLevelTwo()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(3);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(2);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void LoadLevelThree()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(4);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(3);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void LoadLevelFour()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(5);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(4);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void LoadLevelFive()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(6);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(5);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void LoadTutorial()
    {
        FindObjectOfType<AudioManagerScript>().PlayButtonStartSound();
        SceneManager.LoadScene(7);
        FindObjectOfType<GameManagerScript>().SetCurrentStage(0);
        FindObjectOfType<GameManagerScript>().SetScore(15);
    }

    public void ResetProgress()
    {
        FindObjectOfType<GameManagerScript>().ResetPlayerPrefs();
        LoadLevelselectScene();
    }



    //Quit the game
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit Game");
    }


    public void SetLevelStickers()
    {
        GameManagerScript gameManagerScript = FindObjectOfType<GameManagerScript>();

        //Level One
        if (gameManagerScript.GetScoreFrom(1) == 15)
        {
            _imageLevel1.color = Color.white;
            _imageLevel1.sprite = _cocktail1_4;
        }
        if (gameManagerScript.GetScoreFrom(1) < 15 && gameManagerScript.GetScoreFrom(1) >= 12)
        {
            _imageLevel1.color = Color.white;
            _imageLevel1.sprite = _cocktail1_3;

        }
        if (gameManagerScript.GetScoreFrom(1) < 12 && gameManagerScript.GetScoreFrom(1) >= 9)
        {
            _imageLevel1.color = Color.white;
            _imageLevel1.sprite = _cocktail1_2;
        }

        if (gameManagerScript.GetScoreFrom(1) < 9 && gameManagerScript.GetScoreFrom(1) > 1)
        {
            _imageLevel1.color = Color.white;
            _imageLevel1.sprite = _cocktail1_1;
        }

        //Level Two
        if (gameManagerScript.GetScoreFrom(2) == 15)
        {
            _imageLevel2.color = Color.white;
            _imageLevel2.sprite = _cocktail2_4;
        }
        if (gameManagerScript.GetScoreFrom(2) < 15 && gameManagerScript.GetScoreFrom(2) >= 12)
        {
            _imageLevel2.color = Color.white;
            _imageLevel2.sprite = _cocktail2_3;

        }
        if (gameManagerScript.GetScoreFrom(2) < 12 && gameManagerScript.GetScoreFrom(2) >= 9)
        {
            _imageLevel2.color = Color.white;
            _imageLevel2.sprite = _cocktail2_2;
        }

        if (gameManagerScript.GetScoreFrom(2) < 9 && gameManagerScript.GetScoreFrom(2) > 1)
        {
            _imageLevel2.color = Color.white;
            _imageLevel2.sprite = _cocktail2_1;
        }

        //Level Three
        if (gameManagerScript.GetScoreFrom(3) == 15)
        {
            _imageLevel3.color = Color.white;
            _imageLevel3.sprite = _cocktail3_4;
        }
        if (gameManagerScript.GetScoreFrom(3) < 15 && gameManagerScript.GetScoreFrom(3) >= 12)
        {
            _imageLevel3.color = Color.white;
            _imageLevel3.sprite = _cocktail3_3;

        }
        if (gameManagerScript.GetScoreFrom(3) < 12 && gameManagerScript.GetScoreFrom(3) >= 9)
        {
            _imageLevel3.color = Color.white;
            _imageLevel3.sprite = _cocktail3_2;
        }

        if (gameManagerScript.GetScoreFrom(3) < 9 && gameManagerScript.GetScoreFrom(3) > 1)
        {
            _imageLevel3.color = Color.white;
            _imageLevel3.sprite = _cocktail3_1;
        }

        //Level Four
        if (gameManagerScript.GetScoreFrom(4) == 15)
        {
            _imageLevel4.color = Color.white;
            _imageLevel4.sprite = _cocktail4_4;
        }
        if (gameManagerScript.GetScoreFrom(4) < 15 && gameManagerScript.GetScoreFrom(4) >= 12)
        {
            _imageLevel4.color = Color.white;
            _imageLevel4.sprite = _cocktail4_3;

        }
        if (gameManagerScript.GetScoreFrom(4) < 12 && gameManagerScript.GetScoreFrom(4) >= 9)
        {
            _imageLevel4.color = Color.white;
            _imageLevel4.sprite = _cocktail4_2;
        }

        if (gameManagerScript.GetScoreFrom(4) < 9 && gameManagerScript.GetScoreFrom(4) > 1)
        {
            _imageLevel4.color = Color.white;
            _imageLevel4.sprite = _cocktail4_1;
        }

        //Level Five
        if (gameManagerScript.GetScoreFrom(5) == 15)
        {
            _imageLevel5.color = Color.white;
            _imageLevel5.sprite = _cocktail5_4;
        }
        if (gameManagerScript.GetScoreFrom(5) < 15 && gameManagerScript.GetScoreFrom(5) >= 12)
        {
            _imageLevel5.color = Color.white;
            _imageLevel5.sprite = _cocktail5_3;

        }
        if (gameManagerScript.GetScoreFrom(5) < 12 && gameManagerScript.GetScoreFrom(5) >= 9)
        {
            _imageLevel5.color = Color.white;
            _imageLevel5.sprite = _cocktail5_2;
        }

        if (gameManagerScript.GetScoreFrom(5) < 9 && gameManagerScript.GetScoreFrom(5) > 1)
        {
            _imageLevel5.color = Color.white;
            _imageLevel5.sprite = _cocktail5_1;
        }


    }
}
