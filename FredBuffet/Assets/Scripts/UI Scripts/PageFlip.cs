﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageFlip : MonoBehaviour
{
    [SerializeField] private GameObject _page12;
    [SerializeField] private GameObject _page34;
    [SerializeField] private GameObject _page56;

    [SerializeField] private GameObject _book1;
    [SerializeField] private GameObject _book2;

    private Animator _flip;
    private Animator _reverseFlip;

    private void Start()
    {
        _reverseFlip = _book1.GetComponent<Animator>();
        _flip = _book2.GetComponent<Animator>();
    }

    public void Flip()
    {
        //Book 2
        FindObjectOfType<AudioManagerScript>().PlayPageTurnSound();
        StartCoroutine(FlipPageNormal());
        Debug.Log("Flip");
    }

    public void ReverseFlip()
    {
        //Book 1
        FindObjectOfType<AudioManagerScript>().PlayPageTurnSound();
        StartCoroutine(FlipPageReverse());
        Debug.Log("ReverseFlip");

    }
    public void ToPage12()
    {
        StartCoroutine(GoToPage12());
    }

    public void ToPage34()
    {
        StartCoroutine(GoToPage34());
    }

    public void ToPage56()
    {
        StartCoroutine(GoToPage56());
    }

    IEnumerator FlipPageReverse()
    {
        _reverseFlip.SetBool("ReverseFlip", true);
        yield return new WaitForSeconds(0.5f);
        _reverseFlip.SetBool("ReverseFlip", false);
    }

    IEnumerator FlipPageNormal()
    {
        _flip.SetBool("Flip", true);
        yield return new WaitForSeconds(0.5f);
        _flip.SetBool("Flip", false);
    }

    IEnumerator GoToPage34()
    {
        _page12.SetActive(false);
        _page56.SetActive(false);
        yield return new WaitForSeconds(0.6f);
        _page34.SetActive(true);
    }

    IEnumerator GoToPage12()
    {
        _page34.SetActive(false);
        _page56.SetActive(false);
        yield return new WaitForSeconds(0.6f);
        _page12.SetActive(true);
    }
    IEnumerator GoToPage56()
    {
        _page34.SetActive(false);
        _page12.SetActive(false);
        yield return new WaitForSeconds(0.6f);
        _page56.SetActive(true);
    }


}
