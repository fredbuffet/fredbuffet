﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{
    Rigidbody2D rb;
    CircleCollider2D circleCollider;

    bool isCutting = false;
    private GameObject _knife;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        circleCollider = GetComponent<CircleCollider2D>();
        _knife = transform.GetChild(0).gameObject;
        
    }

    void Update()
    {

       if(GameObject.FindGameObjectWithTag("SliceAble") != null)
        {
            _knife.SetActive(true);
        }
        else
        {
            _knife.SetActive(false);
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    StartCutting();
                    break;

                case TouchPhase.Moved:
                    if (isCutting)
                        circleCollider.enabled = true;
                        rb.MovePosition(new Vector2(touchPos.x, touchPos.y));
                    break;

                case TouchPhase.Ended:
                    StopCutting();
                    break;

                default:
                    break;
            }
        }
    }

    void StartCutting()
    {
        isCutting = true;
    }


    void StopCutting()
    {
        circleCollider.enabled = false;
        isCutting = false;
    }

}

