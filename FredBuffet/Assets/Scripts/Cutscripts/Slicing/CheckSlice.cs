﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSlice : MonoBehaviour {

    public bool IsSliced = false;

    private float _timePassed;

    private void Update()
    {
        _timePassed += Time.deltaTime; 

        if(_timePassed >= 2.0f)
        {
            IsSliced = false;
        }
    }

    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Blade"))
        {
            IsSliced = true;
            _timePassed = 0.0f;
        }
    }
}
