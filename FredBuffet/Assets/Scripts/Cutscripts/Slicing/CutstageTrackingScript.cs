﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CutstageTrackingScript : MonoBehaviour {

    [SerializeField] private DictionaryOfStringAndInt _ingredientsNeeded = new DictionaryOfStringAndInt();
    [SerializeField] private DictionaryOfStringAndInt _ingredientsCut = new DictionaryOfStringAndInt();
    [SerializeField] private int _neededLemon = 0;
    [SerializeField] private int _neededOrange = 0;
    [SerializeField] private int _neededGrapefruit = 0;
    [SerializeField] private int _neededMango = 0;
    [SerializeField] private int _neededPineapple = 0;
    [SerializeField] private int _neededLime = 0;

    public bool CuttingDone = false;

    public GameObject NeededHolder;

    private GameManagerScript _gameManager;

    public GameObject NextButton;
    public GameObject Blade;

    private float _timePassed = 0.0f;

    // Use this for initialization
    void Start () {
        _ingredientsNeeded.Add("Lemon", _neededLemon);
        _ingredientsNeeded.Add("Orange", _neededOrange);
        _ingredientsNeeded.Add("Grapefruit", _neededGrapefruit);
        _ingredientsNeeded.Add("Mango", _neededMango);
        _ingredientsNeeded.Add("Pineapple", _neededPineapple);
        _ingredientsNeeded.Add("Lime", _neededLime);

        InitializeIngredientsCut();

        _gameManager = FindObjectOfType<GameManagerScript>();

        InitializeNeeded();
    }

    private void Update()
    {
        if (CuttingDone == true)
        {
            _timePassed += Time.deltaTime;

        }

        if (_timePassed >= 1.0f)
        {
            if(GameObject.FindGameObjectWithTag("SliceAble") != null)
            {
                Destroy(GameObject.FindGameObjectWithTag("SliceAble").gameObject);
            }

                NextButton.SetActive(true);
        }
    }

    private bool CheckEqual()
    {
        bool equal = false;
        if (_ingredientsNeeded.Count == _ingredientsCut.Count) // Require equal count.
        {
            equal = true;
            foreach (var pair in _ingredientsNeeded)
            {
                int value;
                if (_ingredientsCut.TryGetValue(pair.Key, out value))
                {
                    // Require value be equal.
                    if (value != pair.Value)
                    {
                        equal = false;
                        break;
                    }
                }
                else
                {
                    // Require key be present.
                    equal = false;
                    break;
                }
            }
        }
        return equal;
    }

    public void CutIngredient(string IngredientName)
    {
        Debug.Log(IngredientName);
        IngredientName = IngredientName.Substring(0, IngredientName.IndexOf("("));
        _ingredientsCut[IngredientName]++;
        if (_ingredientsCut[IngredientName] > _ingredientsNeeded[IngredientName])
        {
            Debug.Log("You Cut Too Much " + IngredientName + "!");
            FailStage();
        }
        else
        {
            FindObjectOfType<GameManagerScript>().SuccesStage();
        }

        if (CheckEqual())
        {
            Debug.Log("Done");
            Blade.SetActive(false);
            CuttingDone = true;
        }
    }

    private void InitializeIngredientsCut()
    {

        for (int i = 0; i < _ingredientsNeeded.Count; i++)
        {
            string key = _ingredientsNeeded.ElementAt(i).Key;
            _ingredientsCut.Add(key, 0);
        }
    }

    private void FailStage()
    {
        _ingredientsCut.Clear(); //too brutal for childern
        _gameManager.FailStage(1);
        InitializeNeeded();
        InitializeIngredientsCut();
    }


    private void InitializeNeeded()
    {
        foreach (Transform child in NeededHolder.transform)
        {
            GameObject.Destroy(child.gameObject);
        }


        for (int i = 0; i < _ingredientsNeeded.Count; i++)
        {
            string key = "Needed" + _ingredientsNeeded.ElementAt(i).Key;
            int value = _ingredientsNeeded.ElementAt(i).Value;

            //Debug.Log(key);
            //Debug.Log(value);
            
            for(int j = 0; j < value; j++)
            {
                GameObject needed = Resources.Load(key) as GameObject;
                GameObject spawnNeeded =  Instantiate(needed);
                spawnNeeded.transform.SetParent(NeededHolder.transform);
                spawnNeeded.transform.localScale = Vector3.one;
            }
        }


    }
}
