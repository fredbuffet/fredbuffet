﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SlicedScript : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    public Sprite Sliced;
    public Transform[] _children;

    public string ThisFruit;

    private LineRenderer _lineR;
    public Vector3[] _positions;

    private CutstageTrackingScript _cutstageTracking;

    private float _timePassed = 0.0f;

    public bool HasBeenSliced = false;

    void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _children = GetComponentsInChildren<Transform>();


        _lineR = gameObject.GetComponent<LineRenderer>();

        _positions = new Vector3[2];
        _positions[0] = _children[2].position;
        _positions[1] = _children[1].position;
        

        _lineR.SetPositions(_positions);

        _cutstageTracking = FindObjectOfType<CutstageTrackingScript>();
    }

    private void Update()
    {
        if (_children[1].GetComponent<CheckSlice>().IsSliced && _children[2].GetComponent<CheckSlice>().IsSliced && _children[3].GetComponent<CheckSlice>().IsSliced && !HasBeenSliced)
        {
            FindObjectOfType<AudioManagerScript>().PlaySliceSound();
            _lineR.enabled = false;
            SliceFruit();
        }
        //_lineR.SetPositions(_positions);

        //add time after cutted
        if (HasBeenSliced)
        {
            _timePassed += Time.deltaTime;

        }


        if (_timePassed >= 1.0f)
        {
            //destroy the items and add to whatever is needed to add
            Destroy(this.gameObject);
        }
    }

    void SliceFruit()
    {
        Debug.Log("slice it");
        _spriteRenderer.sprite = Sliced;
        HasBeenSliced = true;
        Debug.Log(HasBeenSliced);
        Destroy(GameObject.FindGameObjectWithTag(ThisFruit));
        _cutstageTracking.CutIngredient(this.gameObject.name);
    }

}
