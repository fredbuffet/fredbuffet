﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShakeScript : MonoBehaviour {

    private float accelerometerUpdateInterval = 1.0f / 60.0f;
    // The greater the value of LowPassKernelWidthInSeconds, the slower the
    // filtered value will converge towards current input sample (and vice versa).
    float lowPassKernelWidthInSeconds = 1.0f;
    
    float _shakeDetectionThreshold = 2.0f;

    float lowPassFilterFactor;
    Vector3 lowPassValue;

    private GameManagerScript _gameManager;

    [SerializeField]private float change = 0.2f;

    private Vector3 _originalPos;

    public Image Timer;
    public GameObject ShakerOverheating1;
    public GameObject ShakerOverheating2;
    public GameObject ShakerOverheating3;
    public GameObject nextButton;
    public float _Timer;
    
    private int _shakeStopTimer = 0;


    void Start()
    {
        Timer.fillAmount = 0;
        _Timer = Timer.fillAmount;

        _gameManager = FindObjectOfType<GameManagerScript>();


        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        _shakeDetectionThreshold *= _shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
        _originalPos = this.transform.position;
    }

    void Update()
    {
        Vector3 acceleration = Input.acceleration;
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Vector3 deltaAcceleration = acceleration - lowPassValue;

        if (deltaAcceleration.sqrMagnitude >= _shakeDetectionThreshold)
        {
            Handheld.Vibrate();
            _shakeStopTimer = 0;
            change +=  2* (-change);
            //actually shakes
            FindObjectOfType<AudioManagerScript>().PlayShakerSound(true);
            this.transform.position += change * Vector3.up;
            //Debug.Log("Shake event detected at time " + Time.time);
            Timer.fillAmount += 0.01f;
            _Timer += 0.01f;
        }
        else
        {
            _shakeStopTimer++;
            Debug.Log(_shakeStopTimer);

            if (_shakeStopTimer > 15)
            {
                FindObjectOfType<AudioManagerScript>().PlayShakerSound(false);
                
            }
            this.transform.position = _originalPos;
        }
        
        if (_Timer <1.05f && _Timer > 1.0f)
        {
            nextButton.SetActive(true);

            //FindObjectOfType<GameManagerScript>().SuccesStage();

        }

        if (_Timer > 1.2f)
        {
            ShakerOverheating1.SetActive(true);
            if (_Timer > 1.5f)
            {
                ShakerOverheating2.SetActive(true);
                if (_Timer > 1.8f)
                {
                    ShakerOverheating3.SetActive(true);
                    Debug.Log("Warning, Reaching Critical Shake");
                    if (_Timer > 2.1f && _Timer < 2.2f)
                    {
                        FindObjectOfType<AudioManagerScript>().PlayShakerExplosionSound();
                        FailStage();
                    }
                }
            }
        }

    }

    private void FailStage()
    {
        _gameManager.FailStage(3);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
