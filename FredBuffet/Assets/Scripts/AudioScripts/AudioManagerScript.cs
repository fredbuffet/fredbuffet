﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerScript : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceOneShot;
    [SerializeField] private AudioSource _audioSourceContinous;
    [SerializeField] private AudioClip _sndApplause;
    [SerializeField] private AudioClip _sndBlender;
    [SerializeField] private AudioClip _sndButtonBack;
    [SerializeField] private AudioClip _sndButtonNext;
    [SerializeField] private AudioClip _sndButtonStart;
    [SerializeField] private AudioClip _sndCocktailShakerShake;
    [SerializeField] private AudioClip _sndCocktailShakerExplosion;
    [SerializeField] private AudioClip _sndCurtainOpen;
    [SerializeField] private AudioClip _sndDropSolid;
    [SerializeField] private AudioClip _sndFirework;
    [SerializeField] private AudioClip _sndFruitSlice;
    [SerializeField] private AudioClip _sndFruitSpawn;
    [SerializeField] private AudioClip _sndGlassSelect;
    [SerializeField] private AudioClip _sndLiquidPouring;
    [SerializeField] private AudioClip _sndMuddle;
    [SerializeField] private AudioClip _sndTurnPage;

    public bool InTutorial = false;




    public void PlayShakerSound(bool toggle)
    {
        if (!InTutorial && toggle && FindObjectOfType<LevelProgression>().GetCurrentStageName().Equals("ShakeStage"))
        {
            if (_audioSourceContinous.volume < 0.9f)
            {
                _audioSourceContinous.clip = _sndCocktailShakerShake;
            }
            if (!_audioSourceContinous.isPlaying)
            {
                _audioSourceContinous.loop = true;
                _audioSourceContinous.Play();
                _audioSourceContinous.volume = 1.0f;
            }

        }
        if (!InTutorial && !toggle && FindObjectOfType<LevelProgression>().GetCurrentStageName().Equals("ShakeStage"))
        {
            _audioSourceContinous.Pause();
            _audioSourceContinous.volume = 0.0f;
            _audioSourceContinous.clip = null;

        }
    }

    public void PlayCurtainsSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndCurtainOpen);
    }

    public void PlaySliceSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndFruitSlice);
    }

    public void PlayButtonNextSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndButtonNext);
    }

    public void PlayButtonStartSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndButtonStart);
    }

    public void PlayButtonBackSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndButtonBack);
    }


    public void PlayJuicerSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndBlender);
    }

    public void PlayPouringSound(bool toggle)
    {
        if (!InTutorial && toggle && FindObjectOfType<LevelProgression>().GetCurrentStageName().Equals("FillStage"))
        {
            if (_audioSourceContinous.volume < 0.9f)
            {
                _audioSourceContinous.clip = _sndLiquidPouring;
            }
            if (!_audioSourceContinous.isPlaying)
            {
                _audioSourceContinous.loop = true;
                _audioSourceContinous.Play();
                _audioSourceContinous.volume = 1.0f;
            }

        }
        if (!InTutorial && !toggle && FindObjectOfType<LevelProgression>().GetCurrentStageName().Equals("FillStage"))
        {
            _audioSourceContinous.Pause();
            _audioSourceContinous.volume = 0.0f;
            _audioSourceContinous.clip = null;
        }
    }

    public void PlayApplauseSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndApplause);
    }

    public void PlayFireWorksSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = Random.Range(0.7f, 1.3f);
        _audioSourceOneShot.PlayOneShot(_sndFirework);
    }

    public void PlayFruitSpawningSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndFruitSpawn);
    }

    public void PlayShakerExplosionSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndCocktailShakerExplosion);
    }

    public void PlayPageTurnSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndTurnPage);
    }

    public void PlayFinalGlassSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndGlassSelect);
    }

    public void PlayMashingSound()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndMuddle);
    }

    public void PlayDropSolid()
    {
        _audioSourceOneShot.volume = 1.0f;
        _audioSourceOneShot.pitch = 1.0f;
        _audioSourceOneShot.PlayOneShot(_sndDropSolid);
    }

}
