﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSoundScript : MonoBehaviour {

    private int _oldParticlesAmount1 = 0;
    private int _oldParticlesAmount2 = 0;
    private int _oldParticlesAmount3 = 0;
    private int _oldParticlesAmount4 = 0;
    private int _oldParticlesAmount5 = 0;
    private int _oldParticlesAmount6 = 0;
    private int _oldParticlesAmount7 = 0;
    private int _curParticlesAmount1 = 0;
    private int _curParticlesAmount2 = 0;
    private int _curParticlesAmount3 = 0;
    private int _curParticlesAmount4 = 0;
    private int _curParticlesAmount5 = 0;
    private int _curParticlesAmount6 = 0;
    private int _curParticlesAmount7 = 0;

    [SerializeField] private ParticleSystem _particle1;
    [SerializeField] private ParticleSystem _particle2;
    [SerializeField] private ParticleSystem _particle3;
    [SerializeField] private ParticleSystem _particle4;
    [SerializeField] private ParticleSystem _particle5;
    [SerializeField] private ParticleSystem _particle6;
    [SerializeField] private ParticleSystem _particle7;


    void Update () {
        _curParticlesAmount1 = _particle1.particleCount;
        if (_curParticlesAmount1 > _oldParticlesAmount1)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount1 = _curParticlesAmount1;

        _curParticlesAmount2 = _particle2.particleCount;
        if (_curParticlesAmount2 > _oldParticlesAmount2)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount2 = _curParticlesAmount2;

        _curParticlesAmount3 = _particle3.particleCount;
        if (_curParticlesAmount3 > _oldParticlesAmount3)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount3 = _curParticlesAmount3;

        _curParticlesAmount4 = _particle4.particleCount;
        if (_curParticlesAmount4 > _oldParticlesAmount4)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount4 = _curParticlesAmount4;

        _curParticlesAmount5 = _particle5.particleCount;
        if (_curParticlesAmount5 > _oldParticlesAmount5)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount5 = _curParticlesAmount5;

        _curParticlesAmount6 = _particle6.particleCount;
        if (_curParticlesAmount6 > _oldParticlesAmount6)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount6 = _curParticlesAmount6;

        _curParticlesAmount7 = _particle7.particleCount;
        if (_curParticlesAmount7 > _oldParticlesAmount7)
        {
            FindObjectOfType<AudioManagerScript>().PlayFireWorksSound();
        }
        _oldParticlesAmount7 = _curParticlesAmount7;

    }
}
