﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

    [SerializeField] private AudioClip _failSound;
    [SerializeField] private AudioClip _succesSound;
    [SerializeField] private AudioSource _audioSource;

    private void Awake()
    {

    }

    public void FailStage(int SubtractScore)
    {
        PlayerPrefs.SetInt("TempScore", PlayerPrefs.GetInt("TempScore") - SubtractScore);
        if(!_audioSource.isPlaying)        _audioSource.PlayOneShot(_failSound);
    }

    public void SuccesStage()
    {
        if (!_audioSource.isPlaying) _audioSource.PlayOneShot(_succesSound);
    }

    public void SetScore(int score)
    {
        PlayerPrefs.SetInt("TempScore",score);
    }

    public int GetScore()
    {
        return PlayerPrefs.GetInt("TempScore");
    }

    public void SetCurrentStage(int CurrentStage)
    {
        PlayerPrefs.SetInt("CurrentStage",CurrentStage);
    }

    public int GetCurrentStage()
    {
        return PlayerPrefs.GetInt("CurrentStage");
    }

    public void ResetPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }


    public void SaveScoreTo(int Level)
    {
        switch (Level)
        {
            case 1:
                PlayerPrefs.SetInt("ScoreLevel1", PlayerPrefs.GetInt("TempScore"));
                break;
            case 2:
                PlayerPrefs.SetInt("ScoreLevel2", PlayerPrefs.GetInt("TempScore"));
                break;
            case 3:
                PlayerPrefs.SetInt("ScoreLevel3", PlayerPrefs.GetInt("TempScore"));
                break;
            case 4:
                PlayerPrefs.SetInt("ScoreLevel4", PlayerPrefs.GetInt("TempScore"));
                break;
            case 5:
                PlayerPrefs.SetInt("ScoreLevel5", PlayerPrefs.GetInt("TempScore"));
                break;
            case 0:
                break;
        }
    }

    public int GetScoreFrom(int Level)
    {
        switch (Level)
        {
            case 1:
                return PlayerPrefs.GetInt("ScoreLevel1");
            case 2:
                return PlayerPrefs.GetInt("ScoreLevel2");         
            case 3:
                return PlayerPrefs.GetInt("ScoreLevel3");            
            case 4:
                return PlayerPrefs.GetInt("ScoreLevel4");          
            case 5:
                return PlayerPrefs.GetInt("ScoreLevel5");
        }

        return 0;
    }

}
