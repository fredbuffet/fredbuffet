﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DragScript : MonoBehaviour
{
    private Rigidbody2D RB;
    private SpringJoint2D SJ;
    private GameObject temp;

    private void Start()
    {
        temp = GameObject.FindWithTag("Finger");
        SJ = temp.GetComponent<SpringJoint2D>();
        RB = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            if (Input.GetTouch(0).phase == TouchPhase.Began && GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
            {
                if (RB.name.Contains("Bottle") || RB.name.Contains("Muddle"))
                {
                    RB.constraints = RigidbodyConstraints2D.FreezeRotation;
                }
                else
                    RB.constraints = RigidbodyConstraints2D.None;
                SJ.transform.position = touchPos;
                SJ.connectedBody = RB;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                SJ.GetComponent<Rigidbody2D>().MovePosition(touchPos + transform.forward * Time.deltaTime);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                SJ.connectedBody = null;

            }
        }
    }

}


