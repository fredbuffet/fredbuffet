﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitDestroy : MonoBehaviour {

    public bool ShouldBeDestroyed = false;
    [SerializeField] private float _timer;
    [SerializeField] private float _timeToDestroy = 2.0f;


    private void Start()
    {
        _timer = 0.0f;
    }

    void Update () {

        if (ShouldBeDestroyed)
        {
            _timer += Time.deltaTime;
        }
        else if (!ShouldBeDestroyed)
        {
            _timer = 0.0f;
        }


        if(_timer >= _timeToDestroy)
        {
            Destroy(this.gameObject);
        }

	}
}
