﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spawner : MonoBehaviour
{
    public Image radial;
    public GameObject particle; //liquid 1 - 10;
    private int LiquidNumber;
    private bool canSpawn = true;

    void Update()
    {
        SpawnLiquid();
    }

    void SpawnLiquid()
    {
        if (Input.GetButton("Fire1") && canSpawn == true)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(particle, pos, transform.rotation);
            pos -= Random.insideUnitCircle * 0.05f;
            Instantiate(particle, pos, transform.rotation);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(particle, pos, transform.rotation);
            pos -= Random.insideUnitCircle * 0.05f;
            Instantiate(particle, pos, transform.rotation);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(particle, pos, transform.rotation);
        }

        if (radial.fillAmount >= 1)
            canSpawn = false;
    }
}
