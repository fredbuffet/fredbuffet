﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fillMeter : MonoBehaviour
{
    public Image radial;

    // Use this for initialization
    void Start()
    {
        radial.fillAmount = 0;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        radial.fillAmount+=0.001f;
    }
}
