﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiquidFilling : MonoBehaviour
{

    public GameObject LiquidFillingImage;
    public GameObject LiquidTrailStartImage;
    public GameObject LiquidTrailHeightImage;
    public Color LiquidColor;

    //liquidfilling image vars
    public float StartingHeight;
    public float EndingHeight;
    public float Duration = 5;
    private float _timebetween;
    private float _currentHeight;

    //liquidtrail image vars
    public float StartingHeight2;
    public float EndingHeight2;
    public float Duration2 = 0.5f;
    private float _timebetween2;
    private float _currentHeight2;

    private int _pouringStopTimer = 0;
    public bool DoneFilling = true;
    public bool BeginTutorial = true;

    private GameObject _placeHolderBottle;
    private Color _oldColor;
    void Start()
    {
        ResetPouringAnimation();
        _placeHolderBottle = GameObject.FindGameObjectWithTag("PlaceHolder");
        _oldColor = _placeHolderBottle.GetComponent<SpriteRenderer>().color;
    }

    void Update()
    {
        //_pouringStopTimer++;
        //if (_pouringStopTimer > 15)
        //{
        //    FindObjectOfType<AudioManagerScript>().PlayPouringSound(false);
        //}

        //if (Input.GetMouseButtonDown(0))
        //{
        //    DoneFilling = false;
        //}


        if (!DoneFilling)
        {
            PlayPouringAnimation();
        }
        else
        {
            ResetPouringAnimation();
        }

        //size
        LiquidFillingImage.GetComponent<Transform>().localScale = new Vector3(1, _currentHeight, 0);
        LiquidTrailHeightImage.GetComponent<Transform>().localScale = new Vector3(1, _currentHeight2, 0);

        //color
        LiquidFillingImage.GetComponent<SpriteRenderer>().color = LiquidColor;
        LiquidTrailStartImage.GetComponent<SpriteRenderer>().color = LiquidColor;
        LiquidTrailHeightImage.GetComponent<SpriteRenderer>().color = LiquidColor;

        _pouringStopTimer = 0;
    }

    private void PlayPouringAnimation()
    {
        FindObjectOfType<AudioManagerScript>().PlayPouringSound(true);
        //set active
        LiquidFillingImage.SetActive(true);
        LiquidTrailStartImage.SetActive(true);
        LiquidTrailHeightImage.SetActive(true);

        //lerp between heights
        _currentHeight = Mathf.Lerp(StartingHeight, EndingHeight, _timebetween);
        _currentHeight2 = Mathf.Lerp(StartingHeight2, EndingHeight2, _timebetween2);

        if (_timebetween < 1)
        {

            _timebetween += Time.deltaTime / Duration;
        }

        if (_timebetween2 < 1)
        {
            _timebetween2 += Time.deltaTime / Duration2;
        }

        if (_currentHeight >= EndingHeight)
        {
            _placeHolderBottle.GetComponent<SpriteRenderer>().color = _oldColor;
            _placeHolderBottle.GetComponent<SnapLiquidBottle>().InTrigger = false;
            DoneFilling = true;
            BeginTutorial = false;
        }
    }

    private void ResetPouringAnimation()
    {

        FindObjectOfType<AudioManagerScript>().PlayPouringSound(false);


        //set inactive
        LiquidFillingImage.SetActive(false);
        LiquidTrailStartImage.SetActive(false);
        LiquidTrailHeightImage.SetActive(false);

        //reset values
        _currentHeight = 0;
        _timebetween = 0;
        _currentHeight2 = 0;
        _timebetween2 = 0;
    }
}
