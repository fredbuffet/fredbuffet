﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapLiquidBottle : MonoBehaviour
{
    public bool InTrigger;
    public bool AddIngredient;
    private GameObject _bottle;
    [SerializeField]
    private ShakerContentsScripts _shakerScript;
    [SerializeField]
    private LiquidFilling _liquidPouringController;
    [SerializeField]
    private GameObject _blockBottleCollider;

    private void Start()
    {
        _blockBottleCollider.SetActive(false);
        AddIngredient = false;
        InTrigger = false;
    }

    private void Update()
    {
        if (AddIngredient)
        {
            AddToShaker();
            AddIngredient = false;
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("Bottle"))
        {
            _bottle = other.gameObject;
            InTrigger = true;
            _blockBottleCollider.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name.Contains("Bottle"))
        {
            InTrigger = false;
        }
    }

    private void AddToShaker()
    {
        _shakerScript.AddIngredient(_bottle.gameObject.name.Substring(0, _bottle.gameObject.name.LastIndexOf("(")));
        Destroy(_bottle);
        _shakerScript.CountPassedObjects++;
        _blockBottleCollider.SetActive(false);
    }
}




