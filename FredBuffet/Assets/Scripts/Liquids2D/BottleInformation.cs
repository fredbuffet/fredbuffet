﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleInformation : MonoBehaviour
{

    private float _radius = 2;
    private bool _bottleIsInPosition = false;
    private bool _startRotation = false;
    private bool _poorLiquid;
    private GameObject PlaceholderBottle;
    private GameObject LiquidPouringController;
    public Color BottleLiquidColor;
    private Color _oldColor;
    private Transform _liquid;
    private Vector3 _newScale;
    private float _speed = 0.5f;




    void Start()
    {
        _poorLiquid = true;
        _bottleIsInPosition = false;
        _startRotation = false;
        PlaceholderBottle = GameObject.FindGameObjectWithTag("PlaceHolder");
        LiquidPouringController = GameObject.Find("LiquidPouringController");
        _liquid = this.gameObject.transform.GetChild(0);
    }

    void Update()
    {
        //check if distance between bottle and placeholder is in radius and if player releases the bottle && isn't pouring yet
        if (PlaceholderBottle.GetComponent<SnapLiquidBottle>().InTrigger)
        {
            PlaceholderBottle.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            GetComponent<Rigidbody2D>().simulated = false;
            transform.position = PlaceholderBottle.transform.position;
            if (_startRotation == false)
            {
                transform.rotation = PlaceholderBottle.transform.rotation;
                _startRotation = true;
            }
        }
        if (_startRotation && transform.eulerAngles.z < 90)
        {
            transform.Rotate(Vector3.forward * +90 * Time.deltaTime);
        }
        if (_startRotation && transform.eulerAngles.z >= 90)
        {
            transform.eulerAngles = new Vector3(0, 0, 90);
            _bottleIsInPosition = true;
            _startRotation = false;
        }
        if (_bottleIsInPosition)
        {
            if (_liquid.localScale.y > 0)
            {
                _liquid.localScale -= new Vector3(0, 0.075f * Time.deltaTime, 0);
            }
            LiquidPouringController.GetComponent<LiquidFilling>().LiquidColor = BottleLiquidColor;
            LiquidPouringController.GetComponent<LiquidFilling>().DoneFilling = false;
            PlaceholderBottle.GetComponent<SnapLiquidBottle>().InTrigger = false;
            if (_poorLiquid)
            {
                Invoke("AddToShaker", LiquidPouringController.GetComponent<LiquidFilling>().Duration - 0.2f);
                _poorLiquid = false;
            }
            Destroy(this.gameObject, LiquidPouringController.GetComponent<LiquidFilling>().Duration);
        }
    }
    private void AddToShaker()
    {
        PlaceholderBottle.GetComponent<SnapLiquidBottle>().AddIngredient = true;
    }
}
