﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuicerLiquidColorchange : MonoBehaviour
{
    public GameObject ColorLiquidImage;
    public GameObject RotatingBubbleEffectImage;
    public GameObject MovingBubbleEffectImage;

    public Color StartColor;
    public Color FinishedColor;
    private Color _currentColor;
    private float _timebetween;
    public float Duration = 5;
    public float RotationSpeed = 1;
    public float MovingSpeed = 1;

    public bool _JuicerOn = false;
    public GameObject NextButton;

    private GameObject[] _destroyItems;


    private void Start()
    {
        ColorLiquidImage.GetComponent<SpriteRenderer>().color = StartColor;
        NextButton.SetActive(false);
    }
    private void Update()
    {
        if (_JuicerOn == true)
        {
            ChangeColour();
        }
        else ColorLiquidImage.GetComponent<SpriteRenderer>().color = StartColor;

        _destroyItems = GameObject.FindGameObjectsWithTag("JuicerDestroy");
    }

    public void ChangeColour()
    {
        _currentColor = Color.Lerp(StartColor, FinishedColor, _timebetween);

        if (_timebetween < 1)
        {
            RotatingBubbleEffectImage.transform.Rotate(new Vector3(0, 0, 1), Time.deltaTime * RotationSpeed);
            MovingBubbleEffectImage.transform.Translate(new Vector3(0, 1, 0) * MovingSpeed * Time.deltaTime);

            _timebetween += Time.deltaTime / Duration;
        }
        else
        {
            NextButton.SetActive(true);

            foreach (GameObject destroy in _destroyItems)
            {
                Destroy(destroy);
            }

            Destroy(RotatingBubbleEffectImage);
            Destroy(MovingBubbleEffectImage);
        }

        ColorLiquidImage.GetComponent<SpriteRenderer>().color = _currentColor;
    }

    public void PlaySound()
    {
        FindObjectOfType<AudioManagerScript>().PlayJuicerSound();
    }
}
