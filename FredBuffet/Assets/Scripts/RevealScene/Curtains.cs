﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curtains : MonoBehaviour
{
    private Transform _startMarker;
    [SerializeField]
    private Transform _endMarker;
    [SerializeField]
    private GameObject _spotlight;
    private Spotlight _spotScript;
    [SerializeField]
    private GameObject _particles;
    [SerializeField]
    private GameObject _homeButton;

    private float _speed = 5;

    private float _startTime;
    private float _journeyLength;
    public bool OpenCurtains = false;

    private float _counter;

    void Start()
    {
        _particles.SetActive(false);
        _startMarker = this.transform;
        _spotScript = _spotlight.GetComponent<Spotlight>();
        _homeButton.SetActive(false);
        if(FindObjectOfType<GameManagerScript>().GetScore() > FindObjectOfType<GameManagerScript>().GetScoreFrom(FindObjectOfType<GameManagerScript>().GetCurrentStage()))
            FindObjectOfType<GameManagerScript>().SaveScoreTo(FindObjectOfType<GameManagerScript>().GetCurrentStage());
    }
    void Update()
    {
        _counter += 1 * Time.deltaTime;
        Debug.Log(_counter);
        if (Input.GetMouseButton(0) || _counter >= 5)
        {
            if (!OpenCurtains)
            {
                FindObjectOfType<AudioManagerScript>().PlayCurtainsSound();
                FindObjectOfType<AudioManagerScript>().PlayApplauseSound();
            }
            OpenCurtains = true;
            _spotScript.Pressed = true;
            if (OpenCurtains == true)
            {
                _homeButton.SetActive(true);
            }
        }
        if (OpenCurtains == true)
        {
            StartCoroutine(MoveCurtains());
        }
    }

    private IEnumerator MoveCurtains()
    {
        float step = _speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _endMarker.position, step);
        yield return new WaitForSeconds(2f);
        _particles.SetActive(true);
    }




}
