﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spotlight : MonoBehaviour
{
    private float RotateSpeed = 2f;
    private float Radius = 3f;

    private Vector2 _centre;
    private Vector3 _pos;
    private float _angle;

    public bool Moving = false;
    public bool Pressed = false;

    private void Start()
    {
        _centre = this.transform.position;
        _pos = this.transform.position;
        StartCoroutine(CircleSpotlight());
        Moving = true;
    }

    private void Update()
    {
        if (Moving == true)
        {
            StartCoroutine(CircleSpotlight());
        }
        if (Pressed == true || Input.GetMouseButtonDown(0))
        {
            _pos = _centre;
            _pos.z = 39.3f;
            transform.position = _pos;
            Moving = false;
        }
    }

    IEnumerator CircleSpotlight()
    {
        _angle += RotateSpeed * Time.deltaTime;
        var offset = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * Radius;
        _pos = _centre + offset;
        _pos.z = 39.3f;
        transform.position = _pos;
        yield return new WaitForSeconds(1f);
    }
}
