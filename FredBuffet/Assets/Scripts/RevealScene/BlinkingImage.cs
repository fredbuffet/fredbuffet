﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingImage : MonoBehaviour
{

    // this is the UI.Text or other UI element you want to toggle
    public MaskableGraphic imageToToggle;

    public float interval = 1f;
    public float startDelay = 0.5f;
    public bool currentState = true;
    public bool defaultState = true;
    bool isBlinking = false;

    [SerializeField]
    private GameObject _curtains;
    private Curtains _curtainScript;

    void Start()
    {
        _curtainScript = _curtains.GetComponent<Curtains>();
        imageToToggle.enabled = defaultState;
        StartBlink();
    }

    private void Update()
    {
        if (_curtainScript.OpenCurtains == true) Destroy(gameObject);
    }

    private void StartBlink()
    {
        // do not invoke the blink twice - needed if you need to start the blink from an external object
        if (isBlinking)
            return;

        if (imageToToggle != null)
        {
            isBlinking = true;
            InvokeRepeating("ToggleState", startDelay, interval);
        }
    }

    private void ToggleState()
    {
        imageToToggle.enabled = !imageToToggle.enabled;
    }
}
