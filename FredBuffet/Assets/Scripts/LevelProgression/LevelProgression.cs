﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgression : MonoBehaviour
{
    [Header("Stages")]
    public GameObject[] CutStage;
    public GameObject[] JuiceStage;
    public GameObject[] FillStage;
    public GameObject[] MuddleStage;
    public GameObject[] ShakeStage;
    public GameObject[] SelectStage;
    public GameObject[] FinalStage;

    private GameObject[] _CurrentStage;
    private string _CurrentStageName;

    public GameObject[] FinalCocktails;

    void Start()
    {
        _CurrentStage = CutStage;
        EnableStage(CutStage);
    }

    public void GoToCut()
    {
        DeleteCurrent();
        EnableStage(CutStage);
        SetCurrentStage(CutStage, "CutStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();
    }

    public void GoToJuice()
    {
        DeleteCurrent();
        EnableStage(JuiceStage);
        SetCurrentStage(JuiceStage, "JuiceStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();

    }

    public void GoToFill()
    {
        DeleteCurrent();
        EnableStage(FillStage);
        SetCurrentStage(FillStage, "FillStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();

    }

    public void GoToMuddle()
    {
        DeleteCurrent();
        EnableStage(MuddleStage);
        SetCurrentStage(MuddleStage, "MuddleStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();

    }

    public void GoToShake()
    {
        DeleteCurrent();
        EnableStage(ShakeStage);
        SetCurrentStage(ShakeStage, "ShakeStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();

    }

    public void GoToSelect()
    {
        DeleteCurrent();
        EnableStage(SelectStage);
        SetCurrentStage(SelectStage, "SelectStage");
        FindObjectOfType<AudioManagerScript>().PlayButtonNextSound();

    }


    public void GoToFinalMartini()
    {
        DeleteCurrent();
        EnableStage(FinalStage);
        SetCurrentStage(FinalStage, "FinalStage");
        FinalCocktails[0].SetActive(true);
        FindObjectOfType<AudioManagerScript>().PlayFinalGlassSound();

    }
    public void GoToFinalLongdrink()
    {
        DeleteCurrent();
        EnableStage(FinalStage);
        SetCurrentStage(FinalStage, "FinalStage");
        FinalCocktails[1].SetActive(true);
        FindObjectOfType<AudioManagerScript>().PlayFinalGlassSound();

    }
    public void GoToFinalWhiskey()
    {
        DeleteCurrent();
        EnableStage(FinalStage);
        SetCurrentStage(FinalStage, "FinalStage");
        FinalCocktails[2].SetActive(true);
        FindObjectOfType<AudioManagerScript>().PlayFinalGlassSound();

    }

    private void DeleteCurrent()
    {
        for (int i = 0; i < _CurrentStage.Length; i++)
        {
            _CurrentStage[i].SetActive(false);
        }
    }

    private void EnableStage(GameObject[] stageToEnable)
    {
        for (int i = 0; i < stageToEnable.Length; i++)
        {
            stageToEnable[i].SetActive(true);
        }
    }

    private void SetCurrentStage(GameObject[] stageToSetCurrent, string stageToSetCurrentName)
    {
        _CurrentStage = stageToSetCurrent;
        _CurrentStageName = stageToSetCurrentName;
    }

    public string GetCurrentStageName()
    {
        return _CurrentStageName;
    }
}
