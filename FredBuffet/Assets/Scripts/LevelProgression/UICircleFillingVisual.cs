﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICircleFillingVisual : MonoBehaviour
{
    public Image GrenadineVisual;
    public Image LemonVisual;
    public Image OrangeVisual;

    public GameObject GrenadineButton;
    public GameObject LemonButton;
    public GameObject OrangeButton;

    public Text StuffNeeded;

    public int NeededIceCubes = 2;
    public int NeededSugarCubes = 2;

    public GameObject NextButton;

    public GameObject SugarButton;
    public GameObject IceButton;

    void Start()
    {
        GrenadineVisual.fillAmount = 0;
        LemonVisual.fillAmount = 0;
        OrangeVisual.fillAmount = 0;

        NextButton.SetActive(false);
    }


    private void Update()
    {
        if (GrenadineVisual.fillAmount >= 1 && GrenadineButton.activeInHierarchy)
        {
            GrenadineButton.SetActive(false);
        }

        if (LemonVisual.fillAmount >= 1 && LemonButton.activeInHierarchy)
        {
            LemonButton.SetActive(false);
        }

        if (OrangeVisual.fillAmount >= 1 && OrangeButton.activeInHierarchy)
        {
            OrangeButton.SetActive(false);
        }

        if (NeededIceCubes <= 0 && IceButton.activeInHierarchy)
        {
            IceButton.SetActive(false);
        }

        if (NeededSugarCubes <= 0 && SugarButton.activeInHierarchy)
        {
            SugarButton.SetActive(false);
        }

        StuffNeeded.text = "Fill the shaker: \n" +NeededIceCubes + " more Ice Cubes needed\n " + NeededSugarCubes + " more Sugar Cubes needed";

        if (NeededIceCubes <=0 && NeededSugarCubes <=0 && !GrenadineButton.activeInHierarchy && !LemonButton.activeInHierarchy && !OrangeButton.activeInHierarchy)
        {
            StuffNeeded.text = "Press next to continue";
            NextButton.SetActive(true);
        }
    }


    void OnTriggerEnter2D(Collider2D liquids)
    {
        if (liquids.name
            .Substring(0, liquids.name.IndexOf("(")) == "Grenadine")
        {
            GrenadineVisual.fillAmount += 0.001f;
        }

        if (liquids.name
                .Substring(0, liquids.name.IndexOf("(")) == "LemonJuice")
        {
            LemonVisual.fillAmount += 0.001f;
        }

        if (liquids.name
                .Substring(0, liquids.name.IndexOf("(")) == "OrangeJuice")
        {
            OrangeVisual.fillAmount += 0.001f;
        }

        if (liquids.name.Substring(0, liquids.name.IndexOf("("))== "Sugar")
        {
            NeededSugarCubes--;
        }

        if (liquids.name.Substring(0, liquids.name.IndexOf("(")) == "IceCube")
        {
            NeededIceCubes--;
        }
    }
}
