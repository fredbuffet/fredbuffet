﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuicerFillProgress : MonoBehaviour {
    [SerializeField] private Image _juiceProgress;
    private int _progress = 0;
    private int _progressNeeded = 100;



    private void OnPointerDown()
    {
        _progress++;
        _juiceProgress.fillAmount = _progress / 100.0f;
    }

    public bool GetDone()
    {
        if (_progress >= _progressNeeded)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
