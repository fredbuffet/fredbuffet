﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillShakerChecker : MonoBehaviour
{
    public ShakerContentsScripts SCS;
    public Image Grenadine;
    public GameObject NextButton;


    public GameObject[] DestroyItems;

    // Update is called once per frame
    void Update()
    {

        DestroyItems = GameObject.FindGameObjectsWithTag("JuicerDestroy");

        if (SCS.GetReadyToShake() == true /*&& Grenadine.fillAmount >= 1*/)
        {
            foreach (GameObject other in DestroyItems)
            {
                Destroy(other);
            }

            NextButton.SetActive(true);
        }
    }
}
