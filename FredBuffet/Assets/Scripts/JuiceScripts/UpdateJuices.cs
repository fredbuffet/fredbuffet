﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateJuices : MonoBehaviour {

    public int NeededLemons;
    public int NeededOranges;

    public GameObject LemonButton;
    public GameObject OrangeButton;

    public GameObject NextButton;

    private void Update()
    {
        if (NeededLemons <= 0)
            LemonButton.SetActive(false);

        if (NeededOranges <= 0)
            OrangeButton.SetActive(false);

        if (NeededLemons <= 0 && NeededOranges <= 0)
        {
            NextButton.SetActive(true);
        }
        else NextButton.SetActive(false);
    }
}
