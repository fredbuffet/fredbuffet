﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuicerScript : MonoBehaviour {
    
    private bool _juicesFlowing = false;
    private float _timer = 0.0f;
    [SerializeField] private LiquidSpawningScript _liquidSpawner;
    private string _droppedIngredient;

    private Vector2 _pos;

    GameObject _juicer;

    private void Start()
    {
        _juicer = this.gameObject;
    }


    private void Update()
    {
        if (_juicesFlowing)
        {
            _timer += Time.deltaTime;
            _pos = this.transform.position;
            Instantiate(GetLiquid(_droppedIngredient.Substring(0, _droppedIngredient.IndexOf("("))), _pos, this.transform.rotation);
            _pos += Random.insideUnitCircle * 0.05f;
            Instantiate(GetLiquid(_droppedIngredient.Substring(0,_droppedIngredient.IndexOf("("))),_pos,this.transform.rotation);
            _pos += Random.insideUnitCircle * 0.05f;
            Instantiate(GetLiquid(_droppedIngredient.Substring(0, _droppedIngredient.IndexOf("("))), _pos, this.transform.rotation);
            if (_timer > 3.0f)
            {
                _juicesFlowing = false;
                _timer = 0.0f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entered");
        if (other.CompareTag("JuiceAble"))
        {

            switch (other.gameObject.name)
            {
                case "Lemon(Halved)(Clone)":
                    _juicer.GetComponent<UpdateJuices>().NeededLemons --;
                    Debug.Log(_juicer.GetComponent<UpdateJuices>().NeededLemons);
                    break;
                case "Orange(Halved)(Clone)":
                    _juicer.GetComponent<UpdateJuices>().NeededOranges --;
                    Debug.Log(_juicer.GetComponent<UpdateJuices>().NeededOranges);
                    break;
                default:
                    break;
            }


            _droppedIngredient = other.gameObject.name;
            _juicesFlowing = true;
            Destroy(other.gameObject);

            

        }
    }

    private GameObject GetLiquid(string solidName)
    {
        GameObject liquidGameObject = null;
        switch (solidName)
        {
            case "Grapefruit":
                liquidGameObject = _liquidSpawner.LiquidsArr[1];
                break;
            case "Lemon":
                liquidGameObject = _liquidSpawner.LiquidsArr[5];
                break;
            case "Pineapple":
                liquidGameObject = _liquidSpawner.LiquidsArr[8];
                break;
            case "Orange":
                liquidGameObject = _liquidSpawner.LiquidsArr[6];
                break;
        }



        return liquidGameObject;
    }



}
