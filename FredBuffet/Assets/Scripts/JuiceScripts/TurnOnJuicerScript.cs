﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TurnOnJuicerScript : MonoBehaviour
{
    public GameObject buttonOn;
    public GameObject buttonOff;
    public GameObject top;
    public GameObject neededIngredients;

    public JuicerLiquidColorchange JLC;

    public void Juice()
    {

        if (FindObjectOfType<JuicerContentsScript>().GetReadyToJuice())
        {
            if (JLC._JuicerOn == false)
            {
                ChangeSprite();
            }
        }
        else
        {
            FindObjectOfType<GameManagerScript>().FailStage(0);
        }

    }


    private void ChangeSprite()
    {
        if (buttonOff.activeInHierarchy == true)
        {
            buttonOn.SetActive(true);
            buttonOff.SetActive(false);
            neededIngredients.gameObject.SetActive(false);
            top.SetActive(true);
            JLC._JuicerOn = true;
            JLC.PlaySound();
        }
        else if (buttonOn.activeInHierarchy == true)
        {
            buttonOn.SetActive(false);
            buttonOff.SetActive(true);
            top.SetActive(false);
            neededIngredients.gameObject.SetActive(true);

        }
    }
}
