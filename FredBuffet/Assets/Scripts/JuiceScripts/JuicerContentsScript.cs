﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class JuicerContentsScript : ShakerContentsScripts
{
    //for tutorial
    public static Action TutorialOrange;
    public static Action TutorialLemon;

    [SerializeField] private bool _readyToJuice = false;


    private void Start()
    {
        _ingredientsNeeded.Add("Grapefruit(Peeled)", _neededGrapefruitPeeled);
        _ingredientsNeeded.Add("Grapefruit(Halved)", _neededGrapefruitHalved);
        _ingredientsNeeded.Add("Grapefruit(Whole)", _neededGrapefruitWhole);
        _ingredientsNeeded.Add("Lemon(Halved)", _neededLemonHalved);
        _ingredientsNeeded.Add("Lemon(Whole)", _neededLemonWhole);
        _ingredientsNeeded.Add("Mango(Halved)", _neededMangoHalved);
        _ingredientsNeeded.Add("Mango(Whole)", _neededMangoWhole);
        _ingredientsNeeded.Add("Orange(Halved)", _neededOrangeHalved);
        _ingredientsNeeded.Add("Orange(Whole)", _neededOrangeWhole);
        _ingredientsNeeded.Add("Pineapple(Halved)", _neededPineappleHalved);
        _ingredientsNeeded.Add("Pineapple(Whole)", _neededPineappleWhole);
        _ingredientsNeeded.Add("Raspberry", _neededRaspberry);
        _ingredientsNeeded.Add("Basil", _neededBasil);
        _ingredientsNeeded.Add("Cinnamon", _neededCinnamon);
        _ingredientsNeeded.Add("Mint", _neededMint);
        _ingredientsNeeded.Add("PinkPeppercorn", _neededPinkPeppercorn);
        _ingredientsNeeded.Add("RedBasil", _neededRedBasil);
        _ingredientsNeeded.Add("IceCube(Crushed)", _neededIceCubeCrushed);
        _ingredientsNeeded.Add("IceCube(Whole)", _neededIceCubeWhole);
        _ingredientsNeeded.Add("Salt", _neededSalt);
        _ingredientsNeeded.Add("Sugar", _neededSugar);

        InitializeIngredientsInShaker();

        ContentUpdater();
    }

    private void Update()
    {
        ContentUpdater();
        ForTutorial();
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entered");
        if (!other.CompareTag("Muddler"))
        {
            AddIngredient(other.gameObject.name.Substring(0, other.gameObject.name.LastIndexOf("(")));
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.CompareTag("Muddler"))
        {
            RemoveIngredient(other.gameObject.name.Substring(0, other.gameObject.name.LastIndexOf("(")));
        }
    }


    public override void AddIngredient(string IngredientName)
    {
        Debug.Log(IngredientName);
        FindObjectOfType<AudioManagerScript>().PlayDropSolid();
        _ingredientsInShaker[IngredientName]++;
        if (_ingredientsInShaker[IngredientName] > _ingredientsNeeded[IngredientName])
        {
            Debug.Log("You Added Too Much " + IngredientName + "!");
            FailStage();
            _readyToJuice = false;
        }
        else
        {
            FindObjectOfType<GameManagerScript>().SuccesStage();
        }
        if (CheckEqual())
        {
            Debug.Log("Done");
            _readyToJuice = true;
        }
    }

    public void RemoveIngredient(string IngredientName)
    {
        Debug.Log(IngredientName);
        _ingredientsInShaker[IngredientName]--;
        if (_ingredientsInShaker[IngredientName] > _ingredientsNeeded[IngredientName])
        {
            Debug.Log("You Added Too Much " + IngredientName + "!");
            FailStage();
            _readyToJuice = false;
        }
        if (CheckEqual())
        {
            Debug.Log("Done");
            _readyToJuice = true;
        }

    }

    public override void FailStage()
    {
        FindObjectOfType<GameManagerScript>().FailStage(1);
    }


    public bool GetReadyToJuice()
    {

        return _readyToJuice;
    }

    //for tutorial
    private void ForTutorial()
    {
        if (_ingredientsInShaker["Orange(Halved)"] == 2)
        {
            if (TutorialOrange != null)
                TutorialOrange();
        }
        if (_ingredientsInShaker["Lemon(Halved)"] == 2)
        {
            if (TutorialLemon != null)
                TutorialLemon();
        }
    }

}
