﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateJuiceText : MonoBehaviour {
    private GameObject _juicer;
    public Text NeedText;


    void Start()
    {
        _juicer = this.gameObject;
    }


    void Update()
    {
        NeedText.text = "Juice fruit: \n" + _juicer.GetComponent<UpdateJuices>().NeededLemons + " Lemons\n " + _juicer.GetComponent<UpdateJuices>().NeededOranges + " Oranges";



        if (_juicer.GetComponent<UpdateJuices>().NeededLemons == 0 && _juicer.GetComponent<UpdateJuices>().NeededOranges == 0)
        {
            NeedText.text = "Press next to continue";
        }
    }
}
