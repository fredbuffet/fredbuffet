﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShaker : MonoBehaviour
{
    private float accelerometerUpdateInterval = 1.0f / 60.0f;
    // The greater the value of LowPassKernelWidthInSeconds, the slower the
    // filtered value will converge towards current input sample (and vice versa).
    float lowPassKernelWidthInSeconds = 1.0f;

    float _shakeDetectionThreshold = 2.0f;

    float lowPassFilterFactor;
    Vector3 lowPassValue;


    [SerializeField] private float change = 0.2f;

    private Vector3 _originalPos;

    public float Timer;

    public bool ShakeDone = false;
    private bool _tutDone = false;

    [SerializeField]
    private GameObject _panel;


    void Start()
    {

        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        _shakeDetectionThreshold *= _shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
        _originalPos = this.transform.position;
    }

    void Update()
    {
        if (_panel.activeInHierarchy == false)
        {
            Debug.Log(Timer);
            if (_tutDone == false)
            {
                Vector3 acceleration = Input.acceleration;
                lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
                Vector3 deltaAcceleration = acceleration - lowPassValue;

                if (deltaAcceleration.sqrMagnitude >= _shakeDetectionThreshold)
                {
                    //FindObjectOfType<AudioManagerScript>().PlayShakerSound(true);
                    change += 2 * (-change);
                    //actually shakes
                    this.transform.position += change * Vector3.up;
                    //Debug.Log("Shake event detected at time " + Time.time);
                    Timer += Time.deltaTime;
                }
                else
                {
                    this.transform.position = _originalPos;
                }

                if (Timer > 2.5f && Timer < 3f)
                {
                    //FindObjectOfType<AudioManagerScript>().PlayShakerSound(false);
                    ShakeDone = true;
                    _tutDone = true;
                }
            }
        }
    }

}
