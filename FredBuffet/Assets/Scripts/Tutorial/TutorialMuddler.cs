﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMuddler : MonoBehaviour
{
    private Vector3 _screenPoint;
    private Vector3 _offset;
    private Rigidbody2D _muddle;
    [SerializeField]
    private ParticleSystem _particles;
    public bool MuddlingDone = false;
    public bool MuddlingStart = false;
    private float _counter = 0;
    private bool _startCounting;
    private bool _stopMuddle = false;

    [SerializeField]
    private GameObject _panel;

    void Update()
    {
        if (_panel.activeInHierarchy == false)
        {
            Debug.Log(_counter);
            if (_startCounting == true)
            {
                _counter += Time.deltaTime;
            }
            if (_counter > 8f && _counter < 8.5f)
            {
                MuddlingDone = true;
            }

            if (MuddlingDone == true)
            {
                _stopMuddle = true;
                MuddlingStart = false;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            _startCounting = false;
            _particles.Stop(true);
            MuddlingStart = false;
        }

    }


    void OnMouseDown()
    {
        if (_panel.activeInHierarchy == false)
        {
            _particles.Play(true);
            _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(0, Input.mousePosition.y));
            _muddle = GetComponent<Rigidbody2D>();
            FindObjectOfType<AudioManagerScript>().PlayMashingSound();
        }
    }

    void OnMouseDrag()
    {
        if (_panel.activeInHierarchy == false)
        {
            MuddlingStart = true;
            _startCounting = true;
            if (MuddlingDone == false && _stopMuddle == false)
            {
                Vector3 curScreenPoint = new Vector3(0, Input.mousePosition.y, _screenPoint.z);
                Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
                //transform.position = curPosition;
                _muddle.MovePosition(curPosition);
            }
        }
    }
}

