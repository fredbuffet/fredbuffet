﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class TutorialManager : MonoBehaviour
{
    //bools
    private bool _checkActionCompleted = false;
    private bool _registerMouseClick;
    private bool _actionRequired = false;
    private bool _proceed = false;
    public bool JuicingStage = false;
    public bool ShakerStage = false;
    private bool _fillingDone = false;

    //UI
    [SerializeField]
    private Text _tutorialText;
    [SerializeField]
    private GameObject _bubble;
    [SerializeField]
    private GameObject _panel;
    [SerializeField]
    private GameObject _sideView;

    //stages
    [SerializeField]
    private GameObject _cuttingStage1;
    [SerializeField]
    private GameObject _cuttingStage2;
    [SerializeField]
    private GameObject _cuttingStage3;
    [SerializeField]
    private GameObject _juicingStage;
    [SerializeField]
    private GameObject _fillShakerStage;
    [SerializeField]
    private GameObject _muddlingStage;
    [SerializeField]
    private GameObject _shakingStage;
    [SerializeField]
    private GameObject _endStage;

    //general list
    private List<List<string>> _tutotorialParts;

    //lists sentences
    private List<string> _intro;
    private List<string> _sliceFruit;
    private List<string> _sliceAgain;
    private List<string> _fillJuicer;
    private List<string> _pressButton;
    private List<string> _fillSolids;
    private List<string> _fillLiquids;
    private List<string> _useMuddle;
    private List<string> _shakeShaker;
    private List<string> _outro;

    //For cut stages
    [SerializeField]
    private GameObject _blade;
    private GameObject _lemon;
    private GameObject _orange;
    private SlicedScript _slicedScript;
    private SlicedScript _slicedScript2;
    private bool _lemonIsActive = false;
    private float _lemonCounter;

    //for shaking stage
    [SerializeField]
    private GameObject _shaker;
    private TutorialShaker _shakeScript;

    //for muddling stage
    [SerializeField]
    private GameObject _muddler;
    private TutorialMuddler _muddleScript;

    //for Juicing stage
    [SerializeField]
    private GameObject _juicer;
    [SerializeField]
    private GameObject _turnOnJuicer;
    [SerializeField]
    private GameObject _fruitButtons;
    private JuicerContentsScript _juiceScript;
    private JuicerLiquidColorchange _turnOnScript;
    private bool _juicing = false;
    private float _juiceCounter = 0;
    //colliders
    [SerializeField]
    private GameObject _blockOrange;
    [SerializeField]
    private GameObject _blockLemon;


    [SerializeField]
    private GameObject _orangeButton;
    [SerializeField]
    private GameObject _lemonButton;
    public bool AllOranges = false;
    public bool AllLemons = false;

    //for filling stage
    [SerializeField]
    private GameObject _liquidSpawner;
    [SerializeField]
    private GameObject _triggerFillContent;
    private LiquidFilling _fillingScript;
    private ShakerContentsScripts _contentScripts;
    public bool AllIce;
    public bool AllSugar;
    [SerializeField]
    private GameObject _sugarButton;
    [SerializeField]
    private GameObject _iceButton;
    [SerializeField]
    private GameObject _bottleButton;
    [SerializeField]
    private GameObject _destroyer;
    //colliders
    [SerializeField]
    private GameObject _blockIce;
    [SerializeField]
    private GameObject _blockSugar;

    //send action
    public static Action JuicerIsFilled;
    public static Action ShakerIsFilled;

    void Start()
    {
        _registerMouseClick = false;
        //set stages and object inactive
        _cuttingStage1.SetActive(false);
        _cuttingStage2.SetActive(false);
        _cuttingStage3.SetActive(false);
        _blade.SetActive(false);
        _shakingStage.SetActive(false);
        _juicingStage.SetActive(false);
        _muddlingStage.SetActive(false);
        _fillShakerStage.SetActive(false);
        _endStage.SetActive(false);
        //fill lists
        FillLists();
        //start tutorial
        StartCoroutine(Tutorial());
        //get components
        _shakeScript = _shaker.GetComponent<TutorialShaker>();
        _muddleScript = _muddler.GetComponent<TutorialMuddler>();
        _juiceScript = _juicer.GetComponent<JuicerContentsScript>();
        _turnOnScript = _turnOnJuicer.GetComponent<JuicerLiquidColorchange>();
        _fillingScript = _liquidSpawner.GetComponent<LiquidFilling>();
        _contentScripts = _triggerFillContent.GetComponent<ShakerContentsScripts>();
    }

    private void Update()
    {
        //enable and disable stages
        StartCoroutine(ActivateStages());
        ActionRequired();
        //enable and disable speech
        if (_actionRequired == true)
        {
            _tutorialText.enabled = false;
            _bubble.SetActive(false);
            _panel.SetActive(false);
        }
        if (_actionRequired == false)
        {
            _tutorialText.enabled = true;
            _bubble.SetActive(true);
            _panel.SetActive(true);
        }
        /*//easy skip
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Action Completed");
            _checkActionCompleted = true;
        }*/
        //check completion during stages
        CuttingStage();
        ShakeStage();
        MuddleStage();
        JuiceStage();
        FillStage();
    }

    //Tutorial function
    private IEnumerator Tutorial()
    {
        //tutorial text display
        Debug.Log("Tutorial started");
        foreach (List<string> stage in _tutotorialParts)
        {
            foreach (string sentence in stage)
            {
                //UI text = list text (in CAPS)
                _tutorialText.text = sentence.ToUpper();
                yield return new WaitUntil(() => _registerMouseClick == true);
                _registerMouseClick = false;
                _checkActionCompleted = false;
            }
            yield return new WaitUntil(() => _checkActionCompleted == true);
            _checkActionCompleted = false;
            _actionRequired = false;
            _registerMouseClick = false;
        }
    }

    //Fill lists with strings
    private void FillLists()
    {
        //All tutorial text in lists
        //general list
        _tutotorialParts = new List<List<string>>();

        //Intro list
        _intro = new List<string>();
        _intro.Add("HALLO DAAR, IK BEN BARRY BARMAN EN DIT IS MIJN KEUKEN.");
        _intro.Add("HIER ZULLEN WE SAMEN DE LEKKERSTE DRANKJES MAKEN.");
        _intro.Add("MAAR VOOR WE ERIN VLIEGEN, GEEF IK JE EERST EEN RONDLEIDING DOOR MIJN KEUKEN.");
        _intro.Add("Niets leuker dan samen de kneepjes van het vak leren!");
        _intro.Add("Laten we beginnen!");
        //Select lemon
        _intro.Add("Om een lekker drankje te maken, hebben we vers fruitsap nodig.");
        _intro.Add("Maar voor we het fruit kunnen persen, moeten we het eerst snijden.");
        _intro.Add("Tik op de citroen om hem op je snijplankje te leggen.");
        _tutotorialParts.Add(_intro);

        //Slice lemon
        _sliceFruit = new List<string>();
        _sliceFruit.Add("Goed gedaan! De citroen ligt nu op je snijplankje.");
        _sliceFruit.Add("Nu moeten we hem snijden.");
        _sliceFruit.Add("Veeg met je vinger over de witte lijn om de citroen te snijden.");
        _tutotorialParts.Add(_sliceFruit);

        //Slice orange
        _sliceAgain = new List<string>();
        _sliceAgain.Add("Perfect!");
        _sliceAgain.Add("Nu weet je hoe je een stuk fruit moet snijden.");
        _sliceAgain.Add("Probeer nu hetzelfde met een appelsien.");
        _tutotorialParts.Add(_sliceAgain);


        //Juice Stage lists
        //Fill Juicer
        _fillJuicer = new List<string>();
        _fillJuicer.Add("Dat ziet er goed uit!");
        _fillJuicer.Add("Je bent helemaal klaar voor de volgende stap!");
        _fillJuicer.Add("Ok! Laten we wat fruit persen!");
        _fillJuicer.Add("Gooi 2 citroenen en 2 appelsienen in de juicer.");
        _tutotorialParts.Add(_fillJuicer);

        //Press Button
        _pressButton = new List<string>();
        _pressButton.Add("Goed zo!");
        _pressButton.Add("Nu het fruit in de juicer zit, moet je enkel nog op de knop drukken.");
        _pressButton.Add("Druk op de knop om het fruit te persen.");
        _tutotorialParts.Add(_pressButton);

        //Fill shaker stage
        //Fill solids
        _fillSolids = new List<string>();
        _fillSolids.Add("Dat ziet er lekker uit!");
        _fillSolids.Add("Op naar de volgende stap!");
        _fillSolids.Add("Nu is het tijd om onze shaker te vullen.");
        _fillSolids.Add("Laten we beginnen met de vaste ingredienten");
        _fillSolids.Add("Gooi 2 ijsblokjes en 1 suikerklontje in de shaker.");
        _tutotorialParts.Add(_fillSolids);

        //Fill liquids
        _fillLiquids = new List<string>();
        _fillLiquids.Add("Fantastisch, ik krijg al dorst!");
        _fillLiquids.Add("Nu is het tijd om wat grenadine toe te voegen.");
        _fillLiquids.Add("Sleep het flesje grenadine boven de shaker, het flesje zal vanzelf beginnen gieten!");
        _tutotorialParts.Add(_fillLiquids);

        //Muddling stage
        //muddle ingrediënts
        _useMuddle = new List<string>();
        _useMuddle.Add("Zo, helemaal klaar!");
        _useMuddle.Add("Weer een stapje verder.");
        _useMuddle.Add("Soms moeten we de ingredienten pletten voor we kunnen shaken.");
        _useMuddle.Add("Laten we dat ook even oefenen!");
        _useMuddle.Add("Neem de stamper en stamp de ingredienten in de shaker.");
        _tutotorialParts.Add(_useMuddle);

        //Shake shaker stage
        //shake shaker
        _shakeShaker = new List<string>();
        _shakeShaker.Add("Helemaal klaar!");
        _shakeShaker.Add("Laten we verdergaan.");
        _shakeShaker.Add("Dit is de laatste stap van deze tutorial.");
        _shakeShaker.Add("Nu gaan we alles shaken!");
        _shakeShaker.Add("Schud met je tablet om te shaken.");
        _tutotorialParts.Add(_shakeShaker);

        //Tutorial outro
        _outro = new List<string>();
        _outro.Add("Jij kan goed shaken!");
        _outro.Add("Zo, we hebben het einde van deze tutorial bereikt.");
        _outro.Add("Je bent helemaal klaar voor het maken van je eerste mocktail!");
        _outro.Add("Vergeet niet dat je altijd terug kan komen naar deze tutorial als je hulp nodig hebt.");
        _outro.Add("Klik op het huisje om terug te keren naar het hoofdmenu.");
        _tutotorialParts.Add(_outro);
    }

    //Enable and disable correct stages and objects at the right time
    private IEnumerator ActivateStages()
    {
        //intro
        //Cutting stage1
        yield return new WaitUntil(() => _tutorialText.text == _intro[5].ToUpper());
        _cuttingStage1.SetActive(true);
        _sideView.SetActive(false);

        //cutting stage2
        yield return new WaitUntil(() => _tutorialText.text == _sliceFruit[0].ToUpper());
        _cuttingStage1.SetActive(false);
        _cuttingStage2.SetActive(true);
        yield return new WaitUntil(() => _actionRequired == true);
        _blade.SetActive(true);
        _lemon = GameObject.FindGameObjectWithTag("SliceAble");
        if (_lemon != null)
        {
            _slicedScript = _lemon.GetComponent<SlicedScript>();
            _lemonIsActive = true;
        }
        yield return new WaitUntil(() => _actionRequired == false);
        _blade.SetActive(false);

        //cutting stage3
        yield return new WaitUntil(() => _tutorialText.text == _sliceAgain[0].ToUpper());
        _cuttingStage2.SetActive(false);
        _cuttingStage3.SetActive(true);
        yield return new WaitUntil(() => _actionRequired == true);
        _blade.SetActive(true);
        yield return new WaitUntil(() => _actionRequired == false);
        _blade.SetActive(false);


        //Juicing stage
        yield return new WaitUntil(() => _tutorialText.text == _fillJuicer[2].ToUpper());
        _cuttingStage3.SetActive(false);
        _juicingStage.SetActive(true);
        _sideView.SetActive(true);
        yield return new WaitUntil(() => _tutorialText.text == _pressButton[0].ToUpper());
        _fruitButtons.SetActive(false);


        //shaker filling stage
        yield return new WaitUntil(() => _tutorialText.text == _fillSolids[2].ToUpper());
        _juicingStage.SetActive(false);
        _fillShakerStage.SetActive(true);
        _bottleButton.SetActive(false);
        _liquidSpawner.SetActive(false);
        yield return new WaitUntil(() => _tutorialText.text == _fillLiquids[0].ToUpper());
        _iceButton.SetActive(false);
        _sugarButton.SetActive(false);
        _blockIce.SetActive(false);
        _blockSugar.SetActive(false);
        yield return new WaitUntil(() => _tutorialText.text == _fillLiquids[1].ToUpper());
        _destroyer.SetActive(false);
        _liquidSpawner.SetActive(true);
        _bottleButton.SetActive(true);
        yield return new WaitUntil(() => _tutorialText.text == _useMuddle[0].ToUpper());
        _bottleButton.SetActive(false);
        _liquidSpawner.SetActive(false);
        _destroyer.SetActive(true);

        /*GameObject[] objects = GameObject.FindGameObjectsWithTag("JuicerDestroy");
        foreach (GameObject bottle in objects)
        {
            Destroy(bottle);
        }*/

        //muddling stage
        yield return new WaitUntil(() => _tutorialText.text == _useMuddle[2].ToUpper());
        _fillShakerStage.SetActive(false);
        _muddlingStage.SetActive(true);

        //shaking stage
        yield return new WaitUntil(() => _tutorialText.text == _shakeShaker[0].ToUpper());
        GameObject[] drops = GameObject.FindGameObjectsWithTag("Drop");
        foreach (GameObject drop in drops) GameObject.Destroy(drop);
        yield return new WaitUntil(() => _tutorialText.text == _shakeShaker[2].ToUpper());
        _shakingStage.SetActive(true);
        _muddlingStage.SetActive(false);

        //outro
        yield return new WaitUntil(() => _tutorialText.text == _outro[1].ToUpper());
        _shakingStage.SetActive(false);
        yield return new WaitUntil(() => _tutorialText.text == _outro[_outro.Count - 1].ToUpper());
        yield return new WaitForSecondsRealtime(2f);
        _endStage.SetActive(true);
    }

    //Action is always required at end of each list
    private void ActionRequired()
    {
        foreach (List<string> stage in _tutotorialParts)
        {
            if (_tutorialText.text == stage[stage.Count - 1].ToUpper())
            {
                if (_registerMouseClick == true)
                {
                    Debug.Log("Action Required!");
                    _actionRequired = true;
                }
            }
        }
    }

    //Scene completion via button
    public void SetCompleted()
    {
        _checkActionCompleted = true;
    }

    //Mouseclick check
    public void SetMouseClick()
    {
        _registerMouseClick = true;
    }

    //Check completion for cutting scene
    private void CuttingStage()
    {
        if (_proceed == true)
        {
            _orange = GameObject.FindGameObjectWithTag("SliceAble");
            _slicedScript = _orange.GetComponent<SlicedScript>();
            if (_orange != null && _slicedScript.HasBeenSliced == true)
            {
                Debug.Log("I'm here");
                _checkActionCompleted = true;
                _proceed = false;
            }
        }
        if (_lemonIsActive == true && _slicedScript.HasBeenSliced == true)
        {
            _lemonCounter += Time.deltaTime;
            if (_lemonCounter > 2)
            {
                _checkActionCompleted = true;
                _lemonCounter = 0;
                _lemonIsActive = false;
            }
        }
    }

    //Check completion for shaking stage
    private void ShakeStage()
    {
        if (_shakeScript.ShakeDone == true)
        {
            _checkActionCompleted = true;
            _shakeScript.ShakeDone = false;
        }

    }

    //Check completion for muddle stage
    private void MuddleStage()
    {
        if (_muddleScript.MuddlingDone == true)
        {
            _checkActionCompleted = true;
            _muddleScript.MuddlingDone = false;
        }

    }

    //Check completion for juice stage
    private void JuiceStage()
    {
        if (AllOranges == true)
        {
            //_orangeButton.GetComponent<IngredientSpawnerV3>().DuringTutorial = true;
            _blockOrange.SetActive(true);
        }
        if (AllLemons == true)
        {
            //_lemonButton.GetComponent<IngredientSpawnerV3>().DuringTutorial = true;
            _blockLemon.SetActive(true);
        }
        if (_juiceScript.GetReadyToJuice() == true && _juicing == false)
        {
            JuicingStage = true;
            if (JuicerIsFilled != null)
                JuicerIsFilled();
            _checkActionCompleted = true;
            _juicing = true;
        }
        if (_turnOnScript._JuicerOn == true)
        {
            JuicingStage = false;
            _juiceCounter += Time.deltaTime;
            if (_juiceCounter > 4.5f && _juiceCounter < 5)
            {
                _checkActionCompleted = true;
                AllOranges = false;
                AllLemons = false;
                _juiceCounter = 6;
            }
        }
    }

    //Check completion for fill stage
    private void FillStage()
    {
        if (AllIce == true)
        {
            _blockIce.SetActive(true);
            AllIce = false;
        }
        if (AllSugar == true)
        {
            _blockSugar.SetActive(true);
            AllSugar = false;
        }
        if (_contentScripts.CountPassedObjects >= 3 && _fillingDone == false)
        {
            ShakerStage = true;
            if (ShakerIsFilled != null)
                ShakerIsFilled();
            _checkActionCompleted = true;
            _fillingDone = true;
        }
        if (_fillingScript.DoneFilling == true && _fillingScript.BeginTutorial == false)
        {
            _checkActionCompleted = true;
            _fillingScript.BeginTutorial = true;
        }
    }

    //Proceed for button, proceed, but don't complete action
    public void Proceed()
    {
        _proceed = true;
    }

    private void OnEnable()
    {
        JuicerContentsScript.TutorialOrange += Orange;
        JuicerContentsScript.TutorialLemon += Lemon;
        ShakerContentsScripts.TutorialIce += IceCube;
        ShakerContentsScripts.TutorialSugar += Sugar;
    }

    private void OnDisable()
    {
        JuicerContentsScript.TutorialOrange -= Orange;
        JuicerContentsScript.TutorialLemon -= Lemon;
        ShakerContentsScripts.TutorialIce -= IceCube;
        ShakerContentsScripts.TutorialSugar -= Sugar;
    }

    private void Orange()
    {
        AllOranges = true;
    }
    //count sugar
    private void Lemon()
    {
        AllLemons = true;
    }

    //count cubes
    public void IceCube()
    {
        AllIce = true;
    }
    //count sugar
    public void Sugar()
    {
        AllSugar = true;
    }
}


