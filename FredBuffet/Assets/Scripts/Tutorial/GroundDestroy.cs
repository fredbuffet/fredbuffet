﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDestroy : MonoBehaviour
{
    [SerializeField]
    private TutorialManager _tutor;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<FruitDestroy>() != null && other.gameObject.name == "Orange(Halved)(Clone)" && !_tutor.AllOranges)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
        else if (other.gameObject.GetComponent<FruitDestroy>() != null && other.gameObject.name == "Lemon(Halved)(Clone)" && !_tutor.AllLemons)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
        else if (other.gameObject.GetComponent<FruitDestroy>() != null && other.gameObject.name == "IceCube(Whole)(Clone)" && !_tutor.AllIce)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
        else if (other.gameObject.GetComponent<FruitDestroy>() != null && other.gameObject.name == "Sugar(Clone)" && !_tutor.AllSugar)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
        else if (other.gameObject.GetComponent<FruitDestroy>() != null && other.gameObject.name == "GrenadineBottle(Clone)")
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<FruitDestroy>() != null)
            other.gameObject.GetComponent<FruitDestroy>().ShouldBeDestroyed = false;
    }

}
