﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpeckles : MonoBehaviour
{
    public GameObject[] Drops;
    private bool _spawnDrops;
    private GameObject[] _gameObjects;

    private TutorialMuddler _muddleScript;
    private float _counter;

    private Vector3 _lastPos;

    [SerializeField]
    private Camera _mainCamera;

    [SerializeField]
    private GameObject _panel;

    void Start()
    {
        InvokeRepeating("SpawnWaves", 1.5f, 0.6f);
        _muddleScript = this.GetComponent<TutorialMuddler>();

    }

    private void Update()
    {
        if (_panel.activeInHierarchy == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit;

                Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
                if (hit = Physics2D.Raycast(ray.origin, new Vector2(0, 0)))
                {
                    if (hit.transform.name == "MuddlerTool")
                    {
                        _spawnDrops = true;
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            _spawnDrops = false;
            DestroyAllObjects();
        }

    }
    private void SpawnWaves()
    {

        if (_spawnDrops == true && _muddleScript.MuddlingDone == false)
        {
            Vector3 position = new Vector3(Random.Range(-8.5f, 8.5f), Random.Range(-4.5f, 5f), 90);
            if ((position.x <= _lastPos.x - 1f || position.x >= _lastPos.x + 1f) && (position.y <= _lastPos.y - 1f || position.y >= _lastPos.y + 1f))
            {
                Instantiate(Drops[Random.Range(0, 3)], position, Quaternion.identity);
                _lastPos = position;
            }
        }

    }
    void DestroyAllObjects()
    {
        _gameObjects = GameObject.FindGameObjectsWithTag("Drop");

        for (var i = 0; i < _gameObjects.Length; i++)
        {
            Destroy(_gameObjects[i]);
        }
    }
}
