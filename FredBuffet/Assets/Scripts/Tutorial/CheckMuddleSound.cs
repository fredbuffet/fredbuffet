﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckMuddleSound : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("MuddlerTool"))
        {
            FindObjectOfType<AudioManagerScript>().PlayMashingSound();
        }
    }

}
