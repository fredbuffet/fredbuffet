﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelReference : MonoBehaviour
{
    [SerializeField]
    private GameObject _panel;

    public GameObject Panel()
    {
        return _panel;
    }

}
