﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class TutorialDestroyer : MonoBehaviour
{
    [SerializeField]
    private TutorialManager _tutor;
    private bool _destroyJuice = false;
    private bool _destroyShake = false;


    private void OnTriggerStay2D(Collider2D other)
    {
        if (_destroyJuice && other.tag == "JuicerDestroy" && _tutor.JuicingStage == true)
        {
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
        if (_destroyShake && other.tag == "JuicerDestroy" && _tutor.ShakerStage == true)
        {
            Destroy(other.gameObject);
            other.gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        TutorialManager.JuicerIsFilled += DestroyJuice;
        TutorialManager.ShakerIsFilled += DestroyShake;
    }
    private void OnDisable()
    {
        TutorialManager.JuicerIsFilled -= DestroyJuice;
        TutorialManager.ShakerIsFilled -= DestroyShake;
    }

    private void DestroyShake()
    {
        _destroyShake = true;
    }

    private void DestroyJuice()
    {
        _destroyJuice = true;
    }
}
