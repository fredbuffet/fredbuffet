﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuddleScript : MonoBehaviour
{
    private int _muddles;
    [SerializeField] private int _muddlesNeeded;
    public GameObject NextButton;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Entered");
        if (collision.CompareTag("Muddler"))
        {
            AddMuddle();
        }
    }

    private void Update()
    {
        ActivateButton();
    }

    private void AddMuddle()
    {
        _muddles++;
        FindObjectOfType<AudioManagerScript>().PlayMashingSound();
    }

    public int GetMuddles()
    {
        return _muddles;
    }

    private void ActivateButton()
    {
        if (_muddles >= _muddlesNeeded)
        {
            if(NextButton != null)
                NextButton.SetActive(true);
        }
    }
}
