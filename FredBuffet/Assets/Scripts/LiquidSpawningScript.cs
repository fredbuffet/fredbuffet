﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidSpawningScript : MonoBehaviour
{
    private GameObject _particle;
    public GameObject[] LiquidsArr;
    private string _liquidName;
    public IngredientSpawner _ingSpawner;
    private bool canSpawn = true;

    void Start()
    {
        _ingSpawner = GetComponent<IngredientSpawner>();
    }

    void Update()
    {
        _liquidName = _ingSpawner._selectedLiquidName;
        CheckLiquids();

        if (_particle != null)
        SpawnLiquid();
    }

    void SpawnLiquid()
    {
        if (Input.GetButton("Fire1") && canSpawn == true)//if player can spawn liquid 5 orbs within a sphere around pos will spawn per frame
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(_particle, pos, transform.rotation);
            pos -= Random.insideUnitCircle * 0.05f;
            Instantiate(_particle, pos, transform.rotation);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(_particle, pos, transform.rotation);
            pos -= Random.insideUnitCircle * 0.05f;
            Instantiate(_particle, pos, transform.rotation);
            pos += Random.insideUnitCircle * 0.05f;
            Instantiate(_particle, pos, transform.rotation);
        }
    }

    void CheckLiquids()
    {
        if (_liquidName != "no liquid selected")
        {
            for (int i = 0; i < LiquidsArr.Length; i++)
            {
                if (LiquidsArr[i].name == _liquidName)
                {
                    _particle = LiquidsArr[i];
                }
            }
        }
        else
        {
            _particle = null;
        }
    }
}
